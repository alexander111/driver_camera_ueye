#ifndef CAMUEYE_DRIVER_H
#define CAMUEYE_DRIVER_H

#define NUM_BUFFERS 2

#include "camUeye_defines.h"

#include <ueye.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Timer.h>
#include "stringstacker.h"
#include "stdexcept"
#include <cwchar> //for wchar_t

/* ******** ******** ******/
/* uEye defaults CAMERA PARAMETERS */
/* ******** ******** ******/

namespace Video {

/* ******* ******* ******* *****/
/* uEye CAMERA FUNCTIONS CLASS */
/* ******* ******* ******* *****/
 #ifdef __cplusplus
 extern "C" {
 #endif

class CamUeye_driver
{
    //Q_OBJECT

    // basic params
    HIDS hCam;
    HIDS hIntCam;
    int x_off, y_off, width, height, bpp;
    bool debug;
    // sensor info
    double setFps, setExposureMode;
    double fps, exposure;

    // image buffers
    char*   imgBuffer[NUM_BUFFERS];
    INT     imgBufferId[NUM_BUFFERS];
  

    // auxiliary functions
      int CalcBitsPerPixel(INT ueye_colormode);

     Timer limitTimer;

public:
    explicit CamUeye_driver(int w, int h,
                     int x, int y,
                     double framesPerSec,
                     double SetExposureMode, int hcam_ident = 1);
    CamUeye_driver(int hcam_ident = 1);

    virtual ~CamUeye_driver();
    
    // MainFunctions
    bool startCam( wchar_t *config_filename = L"camera.ini");
    bool stopCam();
    bool snapshot();
    bool startLive();
    void stopLive();
    void EnableDebug(bool bDebug);
    bool GetImage(char *image, ulong *timestamp = NULL);
    bool SetExposure(double expval);
    double getExposure();
    double getFps();
    bool setMasterGain(double gain);
    bool setRGBGains(double rGain, double gGain, double bGain);

    bool getImageSize(int *width, int *height);
};

 
 #ifdef __cplusplus
 }
 #endif

}

#endif // CAMUEYE_DRIVER_H
